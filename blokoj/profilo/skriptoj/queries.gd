extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "profilo"

# Необходимые заголовки
const headers = ["Content-Type: application/json"]


# Запрос на сохранение данных профиля
func save_profile_query(profile):
	return JSON.print({ "query": "mutation { redaktuUniversoUzanto(retnomo: \"%s\", publikigo: true%s) { status message universoUzanto { uuid retnomo } } }" % [profile['nickname'], profile['uuid']] })


# Запрос на сохранение данных профиля
func save_profile_ws(profile, id):
	var query = JSON.print({
		'type': 'start',
		'id': '%s' % id,
		'payload': { "query": "mutation { " +
		"redaktuUniversoUzanto(retnomo: \"%s\", publikigo: true%s) { status message universoUzanto { uuid retnomo } } }" % [profile['nickname'], profile['uuid']] }})
	if Global.logs:
		print('=== save_profile_ws == ',query)
	return query
