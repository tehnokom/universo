extends "res://kerno/fenestroj/tipo_a1.gd"


var konservejo # склад


func load_konservejo():
	if !Global.fenestro_kosmo:
		return
	var sxipo = Global.fenestro_kosmo.get_node('ship')
	if not sxipo:
		return
	konservejo = null
	# находим склад
	for konservejoj in sxipo.get_children():
		if ("objekto_modulo" in konservejoj):
			if konservejoj.objekto_modulo['resurso']['objId']==5: # грузовой модуль
				konservejo = konservejoj
#					print('===konservejo.objekto_modulo==',konservejo.objekto_modulo)
	if !konservejo:
		print('упс, склад не нашли :-(')
		return
	FillItemList()


# выводим, что на складе 
func FillItemList():
	if !konservejo:
		return
	var _items = get_node("VBox/body_texture/ItemList")
	_items.clear()
	var i = 1
	var volumeno = 0
	print('===konservejo=',konservejo.uuid)
	# enteno - содержа́ние (одной субстанции в другой)
	for enteno in konservejo.objekto_modulo['ligilo']['edges']:
		print('===enteno=',enteno['node']['ligilo']['nomo']['enhavo'])
		if enteno['node']['tipo']['objId']==3: # Находится внутри
			_items.add_item(String(i) + ') ' + String(enteno['node']['ligilo']['nomo']['enhavo']) + 
				', внутренний объём:' + String(enteno['node']['ligilo']['volumenoInterna']) + 
				', внешний объём:' + String(enteno['node']['ligilo']['volumenoEkstera']) + 
				', объём хранения:' + String(enteno['node']['ligilo']['volumenoStokado'])
			)
			volumeno += enteno['node']['ligilo']['volumenoStokado']
			i += 1
	if konservejo.objekto_modulo['volumenoInterna']:
		_items.add_item('Всего места: ' + String(konservejo.objekto_modulo['volumenoInterna']))
		_items.add_item('Свободно: ' + String(konservejo.objekto_modulo['volumenoInterna'] - volumeno))
