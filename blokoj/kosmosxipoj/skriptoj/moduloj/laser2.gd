extends Spatial

var uuid #нужно для идентификации конкретного модуля
var objekto_modulo
var integreco # целостность объекта
var potenco # мощность ресурса

export (float) var beam_length = 1000
export (float) var speed_rotation = 3
export (Color, RGBA) var laser_color :Color = Color(1.0,0.0,0.0,1.0)
export var x_limiter: Vector2=Vector2(0.0,90.0)
export var y_limiter: Vector2=Vector2(-90.0,90.0)
var new_target = null # задание новой цели
#если таргет сняли или поменяли до окончания показа выстрела, то выстрел демонстрируем до конца по старой цели
var target = null # ведём цель и стреляем
var livero = 0 # произошел выстрел на 1 перезарядка
var id_livero = [] # id ответов по произведённым выстрелам, уменьшаем кол-во боезапаса

var id_test

var tuttempe_pafo = 50 # втечении времени показывать выстрел
var malfruo = 100 # время задержки выстрела/перезарядка
var kvanto_pafajho = -1 # количество снарядов Если -1 то бесконечный боезапас
#сам лазер отправляет на сервер что он выстрелил и по кому
# обрабатывает результат выстрела оружие - уменьшая кол-во боеприпасов у себя в заряде

var sxipo # указатель на корабль, где установлено данное оружие. Нужно для ведения огня


func _ready():
#	target=get_parent().route #это потом удалить, здесь тестовое получение цели.
	$laser.cast_to.z = -beam_length #устанавливаем максимальную дальность лазера.
	$laser/beam/MeshInstance.mesh.material.set("shader_param/laser_color",laser_color)
	$laser/beam/MeshInstance2.mesh.material.set("shader_param/laser_color",laser_color)

	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('error = ',err)


func _on_data():
	var i_data_server = 0
	for on_data in Net.data_server:
#		print('on_data=', on_data)
#		print('on_data[id] =',on_data['id'],' id_projekto_direkt_del=',id_projekto_direkt_del)
		var index = id_livero.find(int(on_data['id']))
#		print('index=',index,' ::: ', typeof(index), " == ", typeof(on_data['id']))
		if index > -1: # находится в списке выстрелов
#			уменьшаем количество снарядов в оружии
			if kvanto_pafajho>0:
				kvanto_pafajho -= 1
			pass
#			var idx_prj = 0 #индекс массива для удаления
#			for prj in Global.direktebla_objekto[Global.realeco-2]['projekto']['edges']:
#				if prj['node']['uuid']==on_data['payload']['data']['redaktuUniversoProjekto']['universoProjekto']['uuid']:
#					Global.direktebla_objekto[Global.realeco-2]['projekto']['edges'].remove(idx_prj)
#				idx_prj += 1
			id_livero.remove(index)
			Net.data_server.remove(i_data_server)
		elif id_test == int(on_data['id']):
			print('===id_test==',on_data)
			id_test = 0
			Net.data_server.remove(i_data_server)
		i_data_server += 1


func rotate_gun(delta):
	var temp = transform.basis #сохраняем старый базис
#	look_at(target.back().origin,Vector3.UP)#смотрим на цель, это сохранится в текущем базисе, из-за этого и куча движений с буферными базисами
	look_at(target.get_global_transform().origin,Vector3.UP)#смотрим на цель, это сохранится в текущем базисе, из-за этого и куча движений с буферными базисами
	var target_rot = transform.basis#сохраняем целевой базис
	transform.basis = temp#восстанавливаем изначальный базис
	$laser.transform.basis = $laser.transform.basis.slerp(target_rot,speed_rotation*delta) # постепенно поворачиваем к цели
#	$laser.rotation_degrees.x = clamp($laser.rotation_degrees.x,x_limiter.x,x_limiter.y)# ограничиваем вращение пушки вниз, чтобы стреляла только в верхнюю полусферу
#	$laser.rotation_degrees.y = clamp($laser.rotation_degrees.y,y_limiter.x,y_limiter.y)# ограничиваем вращение пушки по сторонам, если нужно
	$gun_body.rotation.y = $laser.rotation.y
	$Turret.rotation.y = $laser.rotation.y


func get_uuid(object):
#	print('==name==',object.name)
	if object.get('uuid'):
#		print('=uuid==',object.uuid)
		return object.uuid
	else:
		return get_uuid(object.get_parent())


# проверка, входит ли uuid в данный объект
#	я могу видеть часть от целого, нужно подняться к предку и проверить его uuid
# проверяем по восходящей до космоса
func sercxado_uuid(parametro_uuid, objekt):
	if objekt.get('uuid'):
		if parametro_uuid == objekt.uuid:
			return true
	if objekt.get_parent().name != 'space':
		return sercxado_uuid(parametro_uuid, objekt.get_parent())
	return false


func _physics_process(delta):
	if target:
#		print('цель задана')
		var length_to_end=beam_length
		var can_shoot = false
		rotate_gun(delta)
		if $laser.is_colliding():
			if sercxado_uuid(target.uuid,$laser.get_collider()):
#				print('стреляем = ',uuid)
	#			print($laser.get_collider().name) #тут можно обработать в кого мы попали и вызвать повреждения.
				length_to_end = get_global_transform().origin.distance_to($laser.get_collision_point())
#				print('тут 1 kvanto_pafajho=',kvanto_pafajho,' tuttempe_pafo=',tuttempe_pafo,' livero=',livero)
				if (tuttempe_pafo>livero) and ((kvanto_pafajho==-1)or(kvanto_pafajho>0)):
					can_shoot = true
					$laser/end_point.visible = true
					$laser/beam.visible = true
					if !livero: # только начали стрелять
						livero = 1 # начало стрельбы после всех проверок
				else:
					$laser/end_point.visible = false
					$laser/beam.visible = false
			else:
				$laser/end_point.visible = false
				$laser/beam.visible = false
		else:
			$laser/end_point.visible = false
			$laser/beam.visible = false
		
		if can_shoot:
			$laser/beam.scale.z=length_to_end
			$laser/end_point.translation.z = -length_to_end
			$laser/end_point/Particles.emitting = true
		else:
			$laser/end_point/Particles.emitting = false
			$laser/beam.scale.z=0.1
			$laser/end_point.translation.z = 0
	else: 
		$laser/end_point.visible = false
		$laser/beam.visible = false
	if livero > 1:
		livero += 1
	if livero == 1: # отправляем выстрел на сервер
#		#Это должно быть только на управляемом корабле, а не на оружии всех
#		if (sxipo)and(sxipo == Global.fenestro_kosmo.get_node('ship')):
#			Global.fenestro_kosmo.get_node('ui_armilo').pafo_server(self, target)
		# сервер отвечает за повреждения объектов
#		 отправляем на сервер повреждение от выстрела
		if Global.server:
			integreco_shanghi(potenco)
		livero = 2
	if livero == 10: # производим разрушение объекта цели
		pass
	if livero > malfruo:
		livero = 0
		target = new_target # меняем цель, если за время выстрели цель поменяли или сняли


const QueryObject = preload("queries.gd")


# отправляем на сервер повреждение от выстрела
func integreco_shanghi(nombro):
#	print('===target.integreco-potenco=',target.integreco-potenco)
	if Global.server:
		var q = QueryObject.new()
		var id = Net.get_current_query_id()
		Net.net_id_clear.append(id)
		if Global.logs:
			print('отправляем на сервер повреждение от выстрела')
		# проверяем, если стреляем не майнинговым лазером
		if objekto_modulo['resurso']['objId']!=11:
			Net.send_json(q.integreco_shanghi(
				target.uuid,
				-nombro,
				0, 0, 0,
				id
			))
			return
		# если не астероид
		if target.objekto['resurso']['objId']!=7: # не простой астероид
			return
		# если астероид
		# если есть руда в астероиде
		# хватит-ли места на все руды на складе корабля?
		print('===nombro=',nombro)
		print('===target.objekto[integreco]=',target.objekto['integreco'])
		print('===target.objekto[volumenoEkstera]=',target.objekto['volumenoEkstera'])
		# вычисляем сколько можем добыть руды от выстрела
		var volumeno = nombro / (target.objekto['integreco'] / target.objekto['volumenoEkstera'])
		print('===volumeno=',volumeno)
		# проверяем, хватит-ли места на все руды на складе корабля
		# находим склад
		var konservejo
		for konservejoj in sxipo.get_children():
			if ("objekto_modulo" in konservejoj):
				if konservejoj.objekto_modulo['resurso']['objId']==5: # грузовой модуль
					konservejo = konservejoj
#					print('===konservejo.objekto_modulo==',konservejo.objekto_modulo)
		if !konservejo:
			print('упс, склад не нашли :-(')
			return
		# суммируем все объекты, лежащие в складе
		var volumeno_stokado = 0
		for kolvo in konservejo.objekto_modulo['ligilo']['edges']:
			if kolvo['node']['tipo']['objId']==3: # Находится внутри
				volumeno_stokado += kolvo['node']['ligilo']['volumenoStokado']
		print('===volumeno_stokado=',volumeno_stokado)
		# свободное пространство в складе
		var volumeno_blanko = konservejo.objekto_modulo['volumenoInterna'] \
			- volumeno_stokado
		print('===volumeno_blanko=',volumeno_blanko)
		if volumeno_blanko <= 0:
			# свободного места на складе нет - выходим
			return
		# коэффициент потерь при складировании
		# случайное число от 0,15 до 0,25
		var koef_stokado = 1 + rand_range(0.15, 0.25)
		print('===koef_stokado=',koef_stokado)
		
		var koeficiento_volumeno = 1 # коэффициент уменьшения добытого ресурса согласно места на складе с учётом потерь хранения
		if volumeno_blanko < (volumeno*koef_stokado): # если на складе места меньше, чем мы можем добыть согласно мощности выстрела
			# нужно уменьшить добычу всех руд на % разницы
			koeficiento_volumeno = volumeno_blanko / (volumeno*koef_stokado)
		print('===koeficiento_volumeno=',koeficiento_volumeno)
		# сколько можем добыть согласно наличия руды в минерале, мощности добычи и места на борту
		var volumeno_max = 0
		if target.objekto['volumenoEkstera'] < volumeno*koeficiento_volumeno: # если руды меньше, чем мы можем добыть и взять на борт
			volumeno_max = target.objekto['volumenoEkstera']
		else:
			volumeno_max = volumeno*koeficiento_volumeno
		print('===volumeno_max=',volumeno_max)
		# объёма астероида
		var volumenoAstEkstera = target.objekto['volumenoEkstera']
		var volumenoAstInterna = target.objekto['volumenoInterna']
		var volumenoAstStokado = target.objekto['volumenoStokado']
		# enteno - содержа́ние (одной субстанции в другой)
		for enteno in target.objekto['ligilo']['edges']: # проходим по всем элементам склада
			# вычисляем сколько добыли руд
#			print('===enteno=',enteno)
				 # находится внутри
			if (enteno['node']['tipo']['objId']==3) and \
					(enteno['node']['ligilo']['volumenoEkstera']>0): # и есть руда
				# количество добытого минерала
				var volumeno_resurso_ekstera = (enteno['node']['ligilo']['volumenoEkstera'] / target.objekto['volumenoEkstera']) * volumeno_max
				var volumeno_resurso_intera = (enteno['node']['ligilo']['volumenoInterna'] / enteno['node']['ligilo']['volumenoEkstera']) * volumeno_resurso_ekstera
				var volumeno_resurso_stokado = (enteno['node']['ligilo']['volumenoStokado'] / enteno['node']['ligilo']['volumenoEkstera']) * volumeno_resurso_ekstera
				volumenoAstEkstera -= volumeno_resurso_ekstera
				volumenoAstInterna -= volumeno_resurso_intera
				volumenoAstStokado -= (enteno['node']['ligilo']['volumenoStokado'] / target.objekto['volumenoStokado']) * volumeno_max
				print('===volumeno_resurso_ekstera=', volumeno_resurso_ekstera, 
					' ==volumeno_resurso_intera=',volumeno_resurso_intera,
					' ==volumeno_resurso_stokado=',volumeno_resurso_stokado)
				# если ресурсов больше, чем места в трюме
#				if enteno['node']['ligilo']['volumenoEkstera'] >= volumeno*koeficiento_volumeno:
#					volumeno_resurso = volumeno*koeficiento_volumeno
#				else:
#					volumeno_resurso = enteno['node']['ligilo']['volumenoEkstera']
				# убавляем минерала астероиду
				var volumenoEkstera = enteno['node']['ligilo']['volumenoEkstera'] - volumeno_resurso_ekstera
				var volumenoInterna = enteno['node']['ligilo']['volumenoInterna'] - volumeno_resurso_intera
				var volumenoStokado = enteno['node']['ligilo']['volumenoStokado'] - volumeno_resurso_stokado
				print('===volumenoEkstera=',volumenoEkstera,
					'  =volumenoInterna=',volumenoInterna,
					'  =volumenoStokado=',volumenoStokado)
				Net.send_json(q.volumeno_shanghi(
					enteno['node']['ligilo']['uuid'],
					volumenoInterna, 
					volumenoEkstera, 
					volumenoStokado
				))
				# увеличиваем объем хранения в нашем корабле на коэффициент складского хранение
				volumeno_resurso_stokado = volumeno_resurso_stokado * koef_stokado
				# прибавляем ресурс на склад своего корабля
				var posedantoId = Global.id
				if Global.fenestro_kosmo.get_node('ship') != sxipo:
					print('===posedantoId=',posedantoId)
					posedantoId = sxipo.objekto['posedantoId']
					print('===posedantoId 2=',posedantoId)
				# проверяем, есть ли на складе такой ресурс
				var uuid_resurso=""
				var resurso = null
				for kolvo in konservejo.objekto_modulo['ligilo']['edges']:
					if (kolvo['node']['tipo']['objId']==3) and \
							(kolvo['node']['ligilo']['resurso']['objId']==enteno['node']['ligilo']['resurso']['objId']): # Находится внутри и одинаковый ресурс
						uuid_resurso = kolvo['node']['ligilo']['uuid']
						resurso = kolvo
				if uuid_resurso: # ресурс найден
					Net.send_json(q.volumeno_shanghi(
						uuid_resurso,
						volumeno_resurso_intera, 
						volumeno_resurso_ekstera, 
						volumeno_resurso_stokado
					))
					# добавляем на склад ресурс
					resurso['node']['ligilo']['volumenoInterna'] = volumeno_resurso_intera
					resurso['node']['ligilo']['volumenoEkstera'] = volumeno_resurso_ekstera
					resurso['node']['ligilo']['volumenoStokado'] = volumeno_resurso_stokado
				else: # такого ресурса нет на корабле
					id_test = Net.get_current_query_id()
					Net.send_json(q.krei_objekto(
						enteno['node']['ligilo']['nomo']['enhavo'], 
						'',
						1, #posedantoTipoId
						1, # posedantoStatusoId
						posedantoId,  #posedantoUzantoSiriusoUzantoId,
						3, #ligiloTipoId, - находится внутри
						konservejo.objekto_modulo['uuid'], # ligiloPosedantoUuid
						enteno['node']['ligilo']['resurso']['objId'], # resursoId
						volumeno_resurso_intera, #volumenoInterna
						volumeno_resurso_ekstera, # volumenoEkstera
						volumeno_resurso_stokado, #volumenoStokado
						id_test
					))
					# добавление на склад корабля ресурса произойдёт по подписке
		# уменьшаем астероиду объёмы
		Net.send_json(q.integreco_shanghi(
			target.uuid,
			-nombro,
			volumenoAstEkstera,
			volumenoAstInterna,
			volumenoAstStokado,
			id
		))

func set_target(targeto):
	if target: # при отмене стрельбы или смена цели
		new_target = targeto
	else: # сюда попадаем только при начале первой стрельбы 
		new_target = targeto
		target = targeto


